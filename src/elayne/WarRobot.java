package elayne;

import robocode.AdvancedRobot;
import robocode.BulletHitEvent;
import robocode.BulletMissedEvent;
import robocode.DeathEvent;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.awt.Color;

public class WarRobot extends AdvancedRobot {
	int turnDirection = 1;

	public void run() {

		setBodyColor(Color.black);
		setGunColor(Color.cyan);
		setBulletColor(Color.orange);
		setBulletColor(Color.red);

		while (true) {
			turnRight(5 * turnDirection);
		}

	}

	public void onScannedRobot(ScannedRobotEvent enemy) {

		double enemyAngle = enemy.getBearing();
		crosshair(enemyAngle);

		if (enemy.getEnergy() < 12 && getEnergy() > 50) {

			setBulletColor(Color.blue);
			fatalShooting(enemy.getEnergy());

		} else if (enemy.getEnergy() > 16 && enemy.getDistance() > 50) {

			setBulletColor(Color.pink);
			fire(2);

		} else if (enemy.getEnergy() > 200 || getEnergy() < 15) {

			setBulletColor(Color.orange);
			fire(1);

		}

		turnRight(90);
		ahead(100);

	}

	public void onHitRobot(HitRobotEvent enemy) {

		double enemyAngle = enemy.getBearing();

		if (enemyAngle > -90 && enemyAngle < 90) {

			crosshair(enemy.getBearing());
			fire(2);
			back(150);

		} else {

			turnRight(enemyAngle);
			fire(3);
			ahead(150);
		}

		scan();

	}

	public void onHitByBullet(HitByBulletEvent enemy) {
		turnRight(normalRelativeAngleDegrees(90 - (getHeading() - enemy.getHeading())));
		ahead(100);
		scan();
	}

	public void onHitWall(HitWallEvent inimigo) {

		if (nearTheWall()) {
			back(100);
		} else {
			ahead(100);
		}
	}

	public void onBulletHit(BulletHitEvent enemy) {

		scan();

		if (enemy.getEnergy() < 50 && getEnergy() > 60 && getGunHeat() > 50) {
			fire(3);
			ahead(this.getHeading() + 100);
		}
	}

	public void onBulletMissed(BulletMissedEvent inimigo) {
		scan();
	}

	public void fatalShooting(double energiaDoInimigo) {

		double shot = (energiaDoInimigo / 4) + .1;
		fire(shot);

	}

	public boolean nearTheWall() {

		return (getX() < 50 || getX() > getBattleFieldWidth() - 50 || getY() < 50
				|| getY() > getBattleFieldHeight() - 50);

	}


	public void crosshair(double enemyAngle) {

		double turnGunAmt = normalRelativeAngleDegrees(enemyAngle + getHeading() - getGunHeading());
		turnGunRight(turnGunAmt);

	}

	public void onWin(WinEvent enemy) {

		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}

	public void onDeath(DeathEvent enemy) {

		System.out.println(getName() + " morreu!");
		System.out.println("Quantidade de inimigos ainda vivos: " + getOthers());

	}

}
